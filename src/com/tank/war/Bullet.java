package com.tank.war;

import java.awt.*;

/**
 * @ClassName Bullet
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/28 21:34
 * @Version 1.0
 **/
public class Bullet {

    private int x = 200, y = 200;

    private final static int SPEED = 10;

    private DirectionEnum direction;

    private TypeEnum type;

    private boolean living = true;

    private TankFrame tankFrame;

    public Rectangle rect = new Rectangle();

    public static final int BULLET_WIDTH = ResourceMgr.bulletD.getWidth(null);
    public static final int BULLET_HEIGHT = ResourceMgr.bulletD.getHeight(null);


    public Bullet(int x, int y, DirectionEnum direction, TypeEnum type, TankFrame tankFrame) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.direction = direction;
        this.tankFrame = tankFrame;

        rect.x = x;
        rect.y = y;
        rect.width = BULLET_WIDTH;
        rect.height = BULLET_HEIGHT;
    }

    public void paintBullet(Graphics g) {
        if (!this.isLive()) {
            tankFrame.bulletList.remove(this);
        }
        switch (direction) {
            case UP:
                g.drawImage(ResourceMgr.bulletU, x, y, null);
                break;
            case DOWN:
                g.drawImage(ResourceMgr.bulletD, x, y, null);
                break;
            case LEFT:
                g.drawImage(ResourceMgr.bulletL, x, y, null);
                break;
            case RIGHT:
                g.drawImage(ResourceMgr.bulletR, x, y, null);
                break;
        }
        moveBullet();
    }

    private void moveBullet() {
        living = true;
        switch (direction) {
            case LEFT:
                x -= SPEED;
                break;
            case RIGHT:
                x += SPEED;
                break;
            case UP:
                y -= SPEED;
                break;
            case DOWN:
                y += SPEED;
                break;
            default:
                break;
        }

        if (x < 0 || y < 0 || x > TankFrame.GAME_WIDTH || y > TankFrame.GAME_HEIGHT) {
            living = false;
        }

        rect.x = this.x;
        rect.y = this.y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public static int getSPEED() {
        return SPEED;
    }

    public boolean isLive() {
        return living;
    }

    public void die() {
        living = false;
    }

    public void setLive(boolean live) {
        this.living = live;
    }

    public DirectionEnum getDirection() {
        return direction;
    }

    public void setDirection(DirectionEnum direction) {
        this.direction = direction;
    }

    public void collision(Tank tank) {
        if (this.type == tank.getType()) return;
        Rectangle bulletRect = new Rectangle(x, y, BULLET_WIDTH, BULLET_HEIGHT);
        Rectangle tankRect = new Rectangle(tank.getX(), tank.getY(), Tank.TANK_WIDTH, Tank.TANK_HEIGHT);
        if (bulletRect.intersects(tankRect)) {
            this.die();
            tank.die();
            tankFrame.explodeList.add(new Explode((tank.getX() + (Tank.TANK_WIDTH / 2 - Explode.EXPLODE_WIDTH / 2)), (tank.getY() + (Tank.TANK_HEIGHT / 2 - Explode.EXPLODE_HEIGHT / 2)), tankFrame));
        }
    }
}
