package com.tank.war;

import java.awt.*;

/**
 * @ClassName Explode
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/31 14:37
 * @Version 1.0
 **/
public class Explode {

    public static final int EXPLODE_WIDTH = ResourceMgr.explodeList[0].getWidth(null);
    public static final int EXPLODE_HEIGHT = ResourceMgr.explodeList[0].getHeight(null);

    private int x = 200, y = 200;

    private TankFrame tankFrame;

    private boolean living = true;

    private int step = 0;

    public Explode(int x, int y, TankFrame tankFrame) {
        this.x = x;
        this.y = y;
        this.tankFrame = tankFrame;
    }

    public void paintExplode(Graphics g) {
        g.drawImage(ResourceMgr.explodeList[step++], x, y, null);
        if (step >= ResourceMgr.explodeList.length)
            tankFrame.explodeList.remove(this);
    }
}
