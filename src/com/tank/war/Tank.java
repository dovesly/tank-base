package com.tank.war;

import java.awt.*;
import java.util.Random;

/**
 * @ClassName Tank
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/28 20:51
 * @Version 1.0
 **/
public class Tank {

    private int x = 200, y = 200;

    private static final int SPEED = 5;

    private boolean move = true;

    private DirectionEnum direction;

    private TypeEnum type;

    private boolean living = true;

    private TankFrame tankFrame;

    private Random random = new Random();

    public Rectangle rect = new Rectangle();

    public static final int TANK_WIDTH = ResourceMgr.badTankU.getWidth(null);
    public static final int TANK_HEIGHT = ResourceMgr.badTankU.getHeight(null);

    public Tank(int x, int y, DirectionEnum direction, TypeEnum type, TankFrame tankFrame) {
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.type = type;
        this.tankFrame = tankFrame;

        rect.x = x;
        rect.y = y;
        rect.width = TANK_WIDTH;
        rect.height = TANK_HEIGHT;
    }

    public void paintTank(Graphics g) {
        if (!this.isLive()) {
            tankFrame.tankList.remove(this);
        }
        switch (direction) {
            case UP:
                g.drawImage(this.type == TypeEnum.GOOD ? ResourceMgr.goodTankU : ResourceMgr.badTankU, x, y, null);
                break;
            case DOWN:
                g.drawImage(this.type == TypeEnum.GOOD ? ResourceMgr.goodTankD : ResourceMgr.badTankD, x, y, null);
                break;
            case LEFT:
                g.drawImage(this.type == TypeEnum.GOOD ? ResourceMgr.goodTankL : ResourceMgr.badTankL, x, y, null);
                break;
            case RIGHT:
                g.drawImage(this.type == TypeEnum.GOOD ? ResourceMgr.goodTankR : ResourceMgr.badTankR, x, y, null);
                break;
        }
        moveTank();


    }

    private boolean isLive() {
        return living;
    }

    private void moveTank() {
        if (!move) return;

        switch (direction) {
            case LEFT:
                x -= SPEED;
                break;
            case RIGHT:
                x += SPEED;
                break;
            case UP:
                y -= SPEED;
                break;
            case DOWN:
                y += SPEED;
                break;
            default:
                break;
        }

        if (this.type == TypeEnum.BAD && random.nextInt(100) > 95)
            this.fire();

        if (this.type == TypeEnum.BAD && random.nextInt(100) > 95)
            randomDir();

        boundsCheck();

        rect.x = this.x;
        rect.y = this.y;
    }

    private void boundsCheck() {
        if (this.x < 2) this.x = 2;
        if (this.y < 28) this.y = 28;
        if (this.x > TankFrame.GAME_WIDTH - Tank.TANK_WIDTH - 2)
            this.x = TankFrame.GAME_WIDTH - Tank.TANK_WIDTH - 2;
        if (this.y > TankFrame.GAME_HEIGHT - Tank.TANK_HEIGHT - 2)
            this.y = TankFrame.GAME_HEIGHT - Tank.TANK_HEIGHT - 2;
    }

    private void randomDir() {
        this.direction = DirectionEnum.values()[random.nextInt(4)];
    }

    public void fire() {
        tankFrame.bulletList.add(new Bullet((x + (TANK_WIDTH / 2 - Bullet.BULLET_WIDTH / 2)), (y + (TANK_HEIGHT / 2 - Bullet.BULLET_HEIGHT / 2)), direction, type, tankFrame));

        if (this.type == TypeEnum.GOOD) new Thread(() -> new Audio("audio/tank_fire.wav").play()).start();
    }

    public boolean isLiving() {
        return living;
    }

    public void die() {
        living = false;
    }


    public void setLiving(boolean living) {
        this.living = living;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public static int getSPEED() {
        return SPEED;
    }

    public boolean isMove() {
        return move;
    }

    public void setMove(boolean move) {
        this.move = move;
    }

    public DirectionEnum getDirection() {
        return direction;
    }

    public void setDirection(DirectionEnum direction) {
        this.direction = direction;
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }
}
