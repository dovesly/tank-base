package com.tank.war;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName TankFrame
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/27 21:25
 * @Version 1.0
 **/
public class TankFrame extends Frame {

    Tank tank = new Tank(200, 400, DirectionEnum.UP, TypeEnum.GOOD,this);

    List<Bullet> bulletList = new ArrayList<>();

    List<Tank> tankList = new ArrayList<>();

    List<Explode> explodeList = new ArrayList<>();

    public static final int GAME_WIDTH = 1024, GAME_HEIGHT = 768;

    TankFrame() {
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        this.setTitle("Tank War");
        this.setResizable(false);
        this.setVisible(true);

        this.addKeyListener(new KeyAdapter() {
            boolean leftFlag = false;
            boolean rightFlag = false;
            boolean upFlag = false;
            boolean downFlag = false;

            @Override
            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();
                switch (key) {
                    case KeyEvent.VK_LEFT:
                        leftFlag = true;
                        break;
                    case KeyEvent.VK_RIGHT:
                        rightFlag = true;
                        break;
                    case KeyEvent.VK_UP:
                        upFlag = true;
                        break;
                    case KeyEvent.VK_DOWN:
                        downFlag = true;
                        break;
                    default:
                        break;
                }
                new Thread(()->new Audio("audio/tank_move.wav").play()).start();
                setTankDirection();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                int key = e.getKeyCode();
                switch (key) {
                    case KeyEvent.VK_LEFT:
                        leftFlag = false;
                        break;
                    case KeyEvent.VK_RIGHT:
                        rightFlag = false;
                        break;
                    case KeyEvent.VK_UP:
                        upFlag = false;
                        break;
                    case KeyEvent.VK_DOWN:
                        downFlag = false;
                        break;
                    case KeyEvent.VK_SPACE:
                        tank.fire();
                        break;
                    default:
                        break;
                }
                setTankDirection();
            }

            void setTankDirection() {
                if (!leftFlag && !rightFlag && !upFlag && !downFlag) {
                    tank.setMove(false);
                } else {
                    if (leftFlag) tank.setDirection(DirectionEnum.LEFT);
                    if (rightFlag) tank.setDirection(DirectionEnum.RIGHT);
                    if (upFlag) tank.setDirection(DirectionEnum.UP);
                    if (downFlag) tank.setDirection(DirectionEnum.DOWN);
                    tank.setMove(true);
                }
            }


        });

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        Color c = g.getColor();
        g.setColor(Color.WHITE);
        g.drawString("当前子弹的数量为：" + bulletList.size(), 10, 60);
        g.drawString("当前敌人的数量为：" + tankList.size(), 10, 80);
        g.drawString("当前爆炸的数量为：" + tankList.size(), 10, 100);
        g.setColor(c);

        // 我军坦克
        tank.paintTank(g);

        // 敌方坦克
        for (int i = 0; i < tankList.size(); i++) {
            tankList.get(i).paintTank(g);
        }

        // 子弹
        for (int i = 0; i < bulletList.size(); i++) {
            bulletList.get(i).paintBullet(g);
        }

        // 爆炸
        for (int i = 0; i < explodeList.size(); i++) {
            explodeList.get(i).paintExplode(g);
        }

        // 子弹碰撞检测
        for (int i = 0; i < bulletList.size(); i++) {
            for (int j = 0; j < tankList.size(); j++) {
                bulletList.get(i).collision(tankList.get(j));
            }
        }
    }

    Image offScreenImage = null;

    @Override
    public void update(Graphics g) {
        if (offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        Graphics gOffScreen = offScreenImage.getGraphics();
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.BLACK);
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);
        paint(gOffScreen);
        g.drawImage(offScreenImage, 0, 0, null);

    }

}
