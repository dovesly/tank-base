package com.tank.war;

/**
 * @ClassName TankMian
 * @Description TODO
 * @Author SLy
 * @Date 2020/10/27 21:24
 * @Version 1.0
 **/
public class TankMian {
    public static void main(String[] args) throws InterruptedException {
        TankFrame tankFrame = new TankFrame();

        // 初始化敌人坦克
        for (int i = 0; i < 10; i++) {
            tankFrame.tankList.add(new Tank(50 + i * 60, 200, DirectionEnum.DOWN, TypeEnum.BAD, tankFrame));
        }

        // new Thread(() -> new Audio("audio/war1.wav").loop()).start();

        while (true) {
            Thread.sleep(50);
            tankFrame.repaint();
        }
    }
}
